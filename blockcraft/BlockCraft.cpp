#include <stdafx.h>

// Constants for application
// Window starting positions
const int       WINDOW_POS_X        = SDL_WINDOWPOS_UNDEFINED;
const int       WINDOW_POS_Y        = SDL_WINDOWPOS_UNDEFINED;
// Window dimensions
const int       WINDOW_WIDTH        = 640;
const int       WINDOW_HEIGHT       = 480;
// Window caption
char*           WINDOW_CAPTION      = "BlockCraft";
// Flags to open the window with
const int       WINDOW_FLAGS        = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
// Set the game FPS
const int       FPS                 = 30;

// Movement directions as integers.
//// Maybe this should be an enum rather than 4 ints?
const int       MOVEMENT_FORWARD    = 0;
const int       MOVEMENT_LEFT       = 1;
const int       MOVEMENT_BACK       = 2;
const int       MOVEMENT_RIGHT      = 3;

// Keep a track of which movement keys have been pressed.
bool movementKeys[4];

// Is the game still running?
bool running = true;

// Initialize the world and game engine.
World world;
GraphicsEngine engine(WINDOW_CAPTION, WINDOW_POS_X, WINDOW_POS_Y, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FLAGS);

// This function initializes the whole application.
void init()
{
    // Make the mouse movement relative to the previous report, not in terms of pixels.
    SDL_SetRelativeMouseMode(SDL_TRUE);
    // Initialize everything
    SDL_Init(SDL_INIT_EVERYTHING);
    // Start the game engine
    engine.Start();
    // Generate the world. (Extremely simple generation script currently...)
    world.Generate();

    // Set the clear colour (background) to opaque black.
    glClearColor(0.0,0.0,0.0,1.0);

    // Make the openGl matrix work in PROJECTION mode. There's a whole bunch of
    // these, but apparently projection is the most suited to most tasks.
    glMatrixMode(GL_PROJECTION);

    // Reset all of the stuff to the original.
    glLoadIdentity();
    // This sets up the perspective (the viewport).
    gluPerspective(45,640.0/480.0,1.0,500.0);
    // Set the matrix to model view mode.
    glMatrixMode(GL_MODELVIEW);
    // Enable several openGl modules like lighting and materials.
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    // We're creating a light here. This is way more complicated than I had hoped.
    // There's a whole equation for this called the Phong Reflection Model that
    // uses three properties called the ambient, specular and diffuse.

    // These lines set up the diffuse and ambient for the aforementioned light.
    float dif[] = {1.0,1.0,1.0,1.0};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, dif);
    float amb[] = {0.2,0.2,0.2,1.0};
    glLightfv(GL_LIGHT0, GL_AMBIENT, amb);
}

// This function is called when we want to draw stuff onto the screen.
void display()
{
    // Firstly, we clear the buffer.
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    // Then reset all translations and stuff to the defaults so we don't have any left-over modifications.
    glLoadIdentity();
    // Set the position of the light.
    float pos[] = {0, 0, 0, 1.0};
    // Set the light up with the position
    glLightfv(GL_LIGHT0, GL_POSITION, pos);

    // Rotate the world by the angles of the player's current perspective
    // (Basically, if they have looked 5 degrees to the left, everything is rotated 5 degrees to the right.)
    glRotatef(world.GetPitch(), 0.5, 0.0, 0.0);
    // Same for yaw. If they've looked up, rotate the world down.
    glRotatef(world.GetYaw(), 0.0, 0.5, 0.0);

    // Move the world around the player.
    glTranslatef(world.GetX(),world.GetY(),world.GetZ());

    // Draw everything!
    world.Draw();
}

// This function is called when the mouse moves.
void mouseMove()
{
    // Get the current yaw and pitch and store them for local use.
    float perspectiveX = world.GetYaw();
    float perspectiveY = world.GetPitch();

    int moveX;
    int moveY;

    // Get the mouse coordinates relative to the previous coordinates, and store them in variables moveX and moveY.
    SDL_GetRelativeMouseState(&moveX, &moveY);

    // Increase the perspective by the amounts changed.
    perspectiveX += moveX;
    perspectiveY += moveY;

    // If either perspective has flipped over 360 degrees or below 0, subtract or add 360 respectively.
    // Not strictly necessary for most cases, because it won't affect how it works after it's been converted
    // into radians, but theoretically the player could look around in circles for several hours and make the 
    // integer flip... 
    if (perspectiveX > 360)
    {
        perspectiveX -= 360;
    }

    if (perspectiveX < 0)
    {
        perspectiveX += 360;
    }

    if (perspectiveY > 360)
    {
        perspectiveY -= 360;
    }

    if (perspectiveY < 0)
    {
        perspectiveY += 360;
    }

    // Then set the new yaw/pitch for the world.
    world.SetYaw(perspectiveX);
    world.SetPitch(perspectiveY);
}

// These sets of functions are hilarious in my opinion.
// Basically, this works because when the player is 'moving forward', what actually is happening
// is that the world is moving back -- because the camera is fixed and cannot move.

// very philosophical.

void moveForward()
{
    world.moveBack();
}

void moveBack()
{
    world.moveForward();
}

void moveRight()
{
    world.moveLeft();
}

void moveLeft()
{
    world.moveRight();
}

// This function is called when the player clicks. The x and y are the coordinates of where they clicked.
// NOTE: This was preliminary work to get the player able to remove/place blocks, but never got it working.
void mouseClick(float x, float y)
{
    // Get the world properties.
    float startX = world.GetX();
    float startY = world.GetY();
    float startZ = world.GetZ();

    float pitch = world.GetPitch();
    float yaw = world.GetYaw();


    // Local storage for later use.
    float endX;
    float endY;
    float endZ;

//    endX = cos(mathRadians(yaw));
//    endZ = sin(mathRadians(yaw));

    // for each block in the world...
    list<Block>::iterator i = world.blocks.begin();
    while(i != world.blocks.end())
    {
        // check if the player clicked it.
        i++;
    }

 //   printf("Mouse clicked at %f,%f\n Perspective: %f, %f\n", x, y, /*z,*/ pitch, yaw);

    // The vector should move 10 blocks in the direction.
    //printf("Vector Start: %f, %f, %f\tVector End: %f, %f, %f\n", x, y +2, z);
    // Print a message showing their pitch and yaw.
    printf("%f, %f\n", pitch, yaw);
}


// Main game loop.
void mainLoop ()
{
    // Keep a track of game ticks for FPS restrainment.
    Uint32 start;
    // Keep a track of events.
    SDL_Event event;
    // save the game time into the start variable.
    start = SDL_GetTicks();

    // Poll for events.
    while(SDL_PollEvent(&event))
    {
        // Handling events:
        switch(event.type)
        {
            // If the player has pressed the X or somehow quit has been called, exit the game
            case SDL_QUIT:
                running = false;
                break;
            // If the player has clicked a mouse button
            case SDL_MOUSEBUTTONDOWN:
                switch (event.button.button)
                {
                    // if the button is the left mouse button
                    case SDL_BUTTON_LEFT:
                        // Call the mouseClick function
                        mouseClick(event.button.x, event.button.y);
                        break;
                }
                break;
            // If the player has moved the mouse
            case SDL_MOUSEMOTION:
                // Call the mouseMove function
                mouseMove();
                break;
            // If the player has pressed a key
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    // If it was the escape key
                    case SDLK_ESCAPE:
                        // Exit the game
                        running = false;
                        break;
                    // if it was W, S, A or D, mark that key as being 'down' in the movementKeys array.
                    case SDLK_w:
                        movementKeys[MOVEMENT_FORWARD] = true;
                        break;
                    case SDLK_s:
                        movementKeys[MOVEMENT_BACK] = true;
                        break;
                    case SDLK_a:
                        movementKeys[MOVEMENT_LEFT] = true;
                        break;
                    case SDLK_d:
                        movementKeys[MOVEMENT_RIGHT] = true;
                        break;
                }
                break;
            // When a key is no longer being pressed
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    // If the key is W, S, A or D, remove them from the list of keys currently being pressed.
                    case SDLK_w:
                        movementKeys[MOVEMENT_FORWARD] = false;
                        break;
                    case SDLK_s:
                        movementKeys[MOVEMENT_BACK] = false;
                        break;
                    case SDLK_a:
                        movementKeys[MOVEMENT_LEFT] = false;
                        break;
                    case SDLK_d:
                        movementKeys[MOVEMENT_RIGHT] = false;
                        break;
                }

        }
    }

    // For each movement key respectively, if it is being pressed, move the player in that direction.
    if (movementKeys[MOVEMENT_FORWARD])
    {
        moveForward();
    }
    if (movementKeys[MOVEMENT_BACK])
    {
        moveBack();
    }
    if (movementKeys[MOVEMENT_LEFT])
    {
        moveLeft();
    }
    if (movementKeys[MOVEMENT_RIGHT])
    {
        moveRight();
    }

    // Swap the game buffers (flip the display buffer basically)
    engine.SwapBuffers();
    // Display new stuff
    display();
    // Limit the FPS so it doesn't go too fast.
    engine.LimitFPS(FPS, start);
}

// This function will exit the game and close all open stuff.
// Prevents memory leakage.
void exit ()
{
    SDL_Quit();
}

// And finally, the main function!
int main(int argc, char** argv)
{
    // Initialize the game
    init();


    // Loop through the game loop
    while(running)
    {
        mainLoop();
    }

    // Exit safely.
    exit();
    return 0;
}

