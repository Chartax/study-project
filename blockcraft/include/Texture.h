#ifndef TEXTURE_H
#define TEXTURE_H

#include <stdafx.h>

using namespace std;

class Texture
{
    public:
        /** Default constructor */
        Texture();
        /** Default destructor */
        virtual ~Texture();

        /** Access m_FilePath
         * \return The current value of m_FilePath
         */
        string GetFilePath() { return m_FilePath; }

        /** Set m_FilePath
         * \param val New value to set
         */
        void SetFilePath(string val) { m_FilePath = val; }

        /** Access m_Id
         * \return The current value of m_Id
         */
        int GetId() { return m_Id; }

        /** Set m_Id
         * \param val New value to set
         */
        void SetId(int val) { m_Id = val; }


    protected:
    private:
        string m_FilePath; //!< Member variable "m_FilePath"
        int m_Id; //!< Member variable "m_Id"
};

#endif // TEXTURE_H
