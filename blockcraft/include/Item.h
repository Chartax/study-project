#ifndef ITEM_H
#define ITEM_H

#include <stdafx.h>
class Item
{
    public:
        /** Default constructor */
        Item();
        /** Default destructor */
        virtual ~Item();
        /** Access m_Type
         * \return The current value of m_Type
         */
        ItemType GetType() { return m_Type; }
        /** Set m_Type
         * \param val New value to set
         */
        void SetType(ItemType val) { m_Type = val; }
        /** Access m_Quantity
         * \return The current value of m_Quantity
         */
        int GetQuantity() { return m_Quantity; }
        /** Set m_Quantity
         * \param val New value to set
         */
        void SetQuantity(int val) { m_Quantity = val; }
        /** Access m_Flags
         * \return The current value of m_Flags
         */
        int GetFlags() { return m_Flags; }
        /** Set m_Flags
         * \param val New value to set
         */
        void SetFlags(int val) { m_Flags = val; }
    protected:
    private:
        ItemType m_Type; //!< Member variable "m_Type"
        int m_Quantity; //!< Member variable "m_Quantity"
        int m_Flags; //!< Member variable "m_Flags"
};

#endif // ITEM_H
