#ifndef ENTITY_H
#define ENTITY_H

#include <stdafx.h>

class Entity
{
    public:
        /** Default constructor */
        Entity();
        /** Default destructor */
        virtual ~Entity();
        /** Access m_X
         * \return The current value of m_X
         */
        float GetX() { return m_X; }
        /** Set m_X
         * \param val New value to set
         */
        void SetX(float val) { m_X = val; }
        /** Access m_Y
         * \return The current value of m_Y
         */
        float GetY() { return m_Y; }
        /** Set m_Y
         * \param val New value to set
         */
        void SetY(float val) { m_Y = val; }
        /** Access m_Z
         * \return The current value of m_Z
         */
        float GetZ() { return m_Z; }
        /** Set m_Z
         * \param val New value to set
         */
        void SetZ(float val) { m_Z = val; }
        /** Access m_Flags
         * \return The current value of m_Flags
         */
        float GetFlags() { return m_Flags; }
        /** Set m_Flags
         * \param val New value to set
         */
        void SetFlags(float val) { m_Flags = val; }
        /** Move the entity
         * \param direction The direction to move the entity in
         * \param magnitude The distance to move the entity by
         */
        void Draw();

        float GetRed() { return m_Red; }

        void SetRed(float val) { m_Red = val; }


        float GetGreen() { return m_Green; }

        void SetGreen(float val) { m_Green = val; }


        float GetBlue() { return m_Blue; }

        void SetBlue(float val) { m_Blue = val; }

    protected:
    private:
        float m_X; //!< Member variable "m_X"
        float m_Y; //!< Member variable "m_Y"
        float m_Z; //!< Member variable "m_Z"
        float m_Flags; //!< Member variable "m_Flags"
        float m_Red;
        float m_Green;
        float m_Blue;
};

#endif // ENTITY_H
