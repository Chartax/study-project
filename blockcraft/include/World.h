#ifndef WORLD_H
#define WORLD_H

#include <stdafx.h>

using namespace std;

class World
{
    public:
        /** Default constructor */
        World();
        /** Default destructor */
        virtual ~World();
        /** Access m_Time
         * \return The current value of m_Time
         */
        float GetTime() { return m_Time; }
        /** Set m_Time
         * \param val New value to set
         */
        void SetTime(float val) { m_Time = val; }
        /** Access m_Engine
         * \return The current value of m_Engine
         */
        Player GetPlayer() { return m_Player; }
        /** Set m_Player
         * \param val New value to set
         */
        void SetPlayer(Player val) { m_Player = val; }
        /** Access m_Flags
         * \return The current value of m_Flags
         */
        int GetFlags() { return m_Flags; }
        /** Set m_Flags
         * \param val New value to set
         */
        void SetFlags(int val) { m_Flags = val; }
        /** Access m_Entities
         * \return The current value of m_Entities
         */
        Entity* GetEntities() { return m_Entities; }
        /** Set m_Entities
         * \param val New value to set
         */
        void SetEntities(Entity* val) { m_Entities = val; }

        void SetX(float val) { m_X = val; }
        float GetX() { return m_X; }
        void SetY(float val) { m_Y = val; }
        float GetY() { return m_Y; }
        void SetZ(float val) { m_Z = val; }
        float GetZ() { return m_Z; }

        void SetYaw(float val) { m_Yaw = val; }
        float GetYaw() { return m_Yaw; }
        void SetPitch(float val) { m_Pitch = val; }
        float GetPitch() { return m_Pitch; }

        void moveForward();
        void moveBack();
        void moveLeft();
        void moveRight();


        void Generate();

        void Draw();

//        GraphicsEngine GetEngine() { return m_Engine; }
//        /** Set m_Engine
//         * \param val New value to set
//         */
//        void SetEngine(GraphicsEngine val) { m_Engine = val; }
//        /** Access m_Engine
//         * \return The current value of m_Engine
//         */
    list<Block> blocks; //!< List of blocks used to generate the world.
    protected:
    private:
        float m_Yaw; // aka perspective-X
        float m_Pitch; // aka perspective-Y
        float m_X;
        float m_Y;
        float m_Z;
        float m_Time; //!< Member variable "m_Time"
        Player m_Player; //!< Member variable "m_Player"
        int m_Flags; //!< Member variable "m_Flags"
        Entity* m_Entities; //!< Member variable "m_Entities"

//        GraphicsEngine m_Engine; //!<Member variable "m_Engine"
};

#endif // WORLD_H
