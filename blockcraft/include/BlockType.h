#ifndef BLOCKTYPE_H
#define BLOCKTYPE_H

#include <stdafx.h>

class BlockType
{
    public:
        /** Default constructor */
        BlockType();
        /** Default destructor */
        virtual ~BlockType();
        /** Access m_TypeName
         * \return The current value of m_TypeName
         */
        string GetTypeName() { return m_TypeName; }
        /** Set m_TypeName
         * \param val New value to set
         */
        void SetTypeName(string val) { m_TypeName = val; }
        /** Access m_BlockId
         * \return The current value of m_BlockId
         */
        int GetBlockId() { return m_BlockId; }
        /** Set m_BlockId
         * \param val New value to set
         */
        void SetBlockId(int val) { m_BlockId = val; }
        /** Access m_Texture
         * \return The current value of m_Texture
         */
        Texture GetTexture() { return m_Texture; }
        /** Set m_Texture
         * \param val New value to set
         */
        void SetTexture(Texture val) { m_Texture = val; }
        /** Access m_Flags
         * \return The current value of m_Flags
         */
        int GetFlags() { return m_Flags; }
        /** Set m_Flags
         * \param val New value to set
         */
        void SetFlags(int val) { m_Flags = val; }
    protected:
    private:
        string m_TypeName; //!< Member variable "m_TypeName"
        int m_BlockId; //!< Member variable "m_BlockId"
        Texture m_Texture; //!< Member variable "m_Texture"
        int m_Flags; //!< Member variable "m_Flags"
};

#endif // BLOCKTYPE_H
