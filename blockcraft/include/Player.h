#ifndef PLAYER_H
#define PLAYER_H

#include <stdafx.h>

class Player : public Entity
{
    public:
        /** Default constructor */
        Player();
        /** Default destructor */
        virtual ~Player();
        /** Access m_Health
         * \return The current value of m_Health
         */
        int GetHealth() { return m_Health; }
        /** Set m_Health
         * \param val New value to set
         */
        void SetHealth(int val) { m_Health = val; }
        /** Access m_Inventory
         * \return The current value of m_Inventory
         */
        Item** GetInventory() { return m_Inventory; }
        /** Set m_Inventory
         * \param val New value to set
         */
        void SetHealth(Item** val) { m_Inventory = val; }
    protected:
    private:
        int m_Health; //!< Member variable "m_Health"
        Item** m_Inventory; //!< Member variable "m_Inventory"
};

#endif // PLAYER_H
