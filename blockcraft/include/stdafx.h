#ifndef STDAFX_H_INCLUDED
#define STDAFX_H_INCLUDED

#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <math.h>
#include <list>
#include <string>

#include <Entity.h>
#include <Texture.h>
#include <ItemType.h>
#include <Item.h>
#include <Player.h>
#include <BlockType.h>
#include <Block.h>
#include <World.h>

#include <GraphicsEngine.h>



#endif // STDAFX_H_INCLUDED
