#ifndef BLOCK_H
#define BLOCK_H

#include <stdafx.h>

class Block: public Entity
{
    public:
        /** Default constructor */
        Block();
        /** Default destructor */
        virtual ~Block();
        /** Access m_Type
         * \return The current value of m_Type
         */
        BlockType GetType() { return m_Type; }
        /** Set m_Type
         * \param val New value to set
         */
        void SetType(BlockType val) { m_Type = val; }
    protected:
    private:
        BlockType m_Type; //!< Member variable "m_Type"
};

#endif // BLOCK_H
