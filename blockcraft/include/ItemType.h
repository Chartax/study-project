#ifndef ITEMTYPE_H
#define ITEMTYPE_H

#include <stdafx.h>

class ItemType
{
    public:
        /** Default constructor */
        ItemType();
        /** Default destructor */
        virtual ~ItemType();
        /** Access m_TypeName
         * \return The current value of m_TypeName
         */
        string GetTypeName() { return m_TypeName; }
        /** Set m_TypeName
         * \param val New value to set
         */
        void SetTypeName(string val) { m_TypeName = val; }
        /** Access m_Texture
         * \return The current value of m_Texture
         */
        Texture GetTexture() { return m_Texture; }
        /** Set m_Texture
         * \param val New value to set
         */
        void SetTexture(Texture val) { m_Texture = val; }
        /** Access m_ItemId
         * \return The current value of m_ItemId
         */
        int GetItemId() { return m_ItemId; }
        /** Set m_ItemId
         * \param val New value to set
         */
        void SetItemId(int val) { m_ItemId = val; }
        /** Access m_Flags
         * \return The current value of m_Flags
         */
        int GetFlags() { return m_Flags; }
        /** Set m_Flags
         * \param val New value to set
         */
        void SetFlags(int val) { m_Flags = val; }
    protected:
    private:
        string m_TypeName; //!< Member variable "m_TypeName"
        Texture m_Texture; //!< Member variable "m_Texture"
        int m_ItemId; //!< Member variable "m_ItemId"
        int m_Flags; //!< Member variable "m_Flags"
};

#endif // ITEMTYPE_H
