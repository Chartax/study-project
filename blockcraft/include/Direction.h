#ifndef DIRECTION_H
#define DIRECTION_H
#include <stdafx.h>

class Direction
{
    public:
        /** Default constructor */
        Direction();
        /** Default destructor */
        virtual ~Direction();
        /** Access m_Pitch
         * \return The current value of m_Pitch
         */
        float GetPitch() { return m_Pitch; }
        /** Set m_Pitch
         * \param val New value to set
         */
        void SetPitch(float val) { m_Pitch = val; }
        /** Access m_Yaw
         * \return The current value of m_Yaw
         */
        float GetYaw() { return m_Yaw; }
        /** Set m_Yaw
         * \param val New value to set
         */
        void SetYaw(float val) { m_Yaw = val; }
    protected:
    private:
        float m_Pitch; //!< Member variable "m_Pitch"
        float m_Yaw; //!< Member variable "m_Yaw"
};

#endif // DIRECTION_H
