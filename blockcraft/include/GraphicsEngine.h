#ifndef GRAPHICSENGINE_H
#define GRAPHICSENGINE_H
#include <stdafx.h>

class GraphicsEngine
{
    public:
        GraphicsEngine(char* title, int x, int y, int w, int h, Uint32 flags);

        virtual ~GraphicsEngine();

        int GetFlags() { return m_Flags; }
        void SetFlags(int val) { m_Flags = val; }

        int GetHeight() { return m_Height; }
        void SetHeight(int val) { m_Height = val; }

        char* GetTitle() { return m_Title; }
        void SetTitle (char* val) { m_Title = val; }

        int GetWidth() { return m_Width; }
        void SetWidth(int val) { m_Width = val; }

        int GetPosY() { return m_PosY; }
        void SetPosY(int val) { m_PosY = val; }

        int GetPosX() { return m_PosX; }
        void SetPosX(int val) { m_PosX = val; }

        Uint32 GetWindowFlags() { return m_WindowFlags; }
        void SetWindowFlags(Uint32 val) { m_WindowFlags = val; }

        SDL_Window* GetWindow() { return m_Window; }

        void SetWindow(SDL_Window* window) { m_Window = window; }

        void SetContext(SDL_GLContext context) { m_Context = context; }

        void SwapBuffers ();

        void LimitFPS(int FPS, Uint32 start);

        void Start ();

    protected:
    private:
        int m_Flags; //!< Member variable "m_Flags"
        SDL_Window* m_Window; //!< Member variable "m_Window"
        SDL_GLContext m_Context; //!< Member variable "m_Context"
        char* m_Title;
        int m_PosX;
        int m_PosY;
        int m_Width;
        int m_Height;
        Uint32 m_WindowFlags;
};

#endif // GRAPHICSENGINE_H
