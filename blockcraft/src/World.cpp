#include <stdafx.h>

using namespace std;

// Helper function to convert degrees into radians.
float mathRadians (float degrees)
{
    // Pi * degrees / 180
    return M_PI * degrees / 180;
}

World::World()
{
}

// Draw the world
void World::Draw()
{
    // Get the list of blocks in the world
    list<Block> blocks = this->blocks;
    // For each block in the world
    list<Block>::iterator i = blocks.begin();
    while(i != blocks.end())
    {
        // Draw that block.
        i->Draw();
        i++;
    }
}

// Generate the world.
void World::Generate()
{
    // Simplest world generation system I could think of.
    this->SetX(0);
    this->SetY(0);
    this->SetZ(0);
    // Generate a 16x16 array of blocks.
    for (int x = -16; x < 16; x++)
    {
        for (int z = -16; z < 16; z++)
        {
            // There was a point where this did it in three dimensions. (16x16x16)
            // So the player was in a box, not on a platform.
            /*if (x == -16)
            {
                for (int y = -2; y = 16; y++)
                {
                    Block block;
                    block.SetX(x);
                    block.SetY(y);
                    block.SetZ(z);
                    this->blocks.push_front(block);
                }
            }
            else
            { */
                // Create a new block
                Block block;
                // Set the dimensions and colors
                block.SetX(x);
                block.SetY(-2);
                block.SetZ(z);
                block.SetRed(1.0);
                block.SetGreen(1.0);
                block.SetBlue(1.0);
                // Push it onto the block list of the world.
                this->blocks.push_front(block);
            // }
        }
    }
}

// Move the world forward

// Note the somewhat confusing formula for working out the end coordinates.
// sin(rad(yaw + 90)) for z (this should probably be cos(rad(yaw)) but whatever.)
// sin(rad(yaw - 180)) for x
void World::moveForward()
{
    // Get the current properties
    float z = this->GetZ();
    float x = this->GetX();
    // Calculate where the world should end up
    z -= 0.1 * sin(mathRadians(this->GetYaw() + 90));
    x -= 0.1 * sin(mathRadians(this->GetYaw() - 180));
    // Move it to the new position.
    this->SetZ(z);
    this->SetX(x);

}

// Move the world back
void World::moveBack()
{
    // Get the current properties
    float z = this->GetZ();
    float x = this->GetX();
    // Calculate where the world should end up
    z += 0.1 * sin(mathRadians(this->GetYaw() + 90));
    x += 0.1 * sin(mathRadians(this->GetYaw() - 180));
    // Move it to the new position.
    this->SetZ(z);
    this->SetX(x);
}

// Move the world right
void World::moveRight()
{
    // Get the current properties
    float z = this->GetZ();
    float x = this->GetX();
    // Calculate where the world should end up
    z += 0.1 * sin(mathRadians(this->GetYaw()));
    x += 0.1 * sin(mathRadians(this->GetYaw() - 270));
    // Move it to the new position.
    this->SetZ(z);
    this->SetX(x);

}

// Move the world left
void World::moveLeft()
{
    // Get the current properties
    float z = this->GetZ();
    float x = this->GetX();
    // Calculate where the world should end up
    z -= 0.1 * sin(mathRadians(this->GetYaw()));
    x -= 0.1 * sin(mathRadians(this->GetYaw() - 270));
    // Move it to the new position.
    this->SetZ(z);
    this->SetX(x);
}

World::~World()
{
    //dtor
}
