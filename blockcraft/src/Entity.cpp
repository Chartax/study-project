// Include precompiled headers.
#include <stdafx.h>

Entity::Entity()
{
    //ctor
}

Entity::~Entity()
{
    //dtor
}

// When the entity is drawn
void Entity::Draw()
{
    // The block is 1x1
    float size = 1.0;

    // Get the position (x, y and z) of the block.
    float offsetX = this->GetX();
    float offsetY = this->GetY();
    float offsetZ = this->GetZ();

    // Get the color (r, g and b) of the block.
    float red = this->GetRed();
    float green = this->GetGreen();
    float blue = this->GetBlue();

    // The diffuse and ambience of the block. 
    // For shinyness in light and stuff. 
    // I would have made this a property eventually, probably
    float difamb[] = {1.0,0.5,0.3,1.0};
    glBegin(GL_QUADS);
    // Draw the various block faces.
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, difamb);
                // Front face
                glNormal3f(0.0,0.0,1.0);
              glColor3f(red,green,blue);
                glVertex3f(size/2 + offsetX, size/2 + offsetY, size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX, size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX, -size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(size/2 + offsetX, -size/2 + offsetY,size/2 + offsetZ);
                // Left face
                glNormal3f(-1.0,0.0,0.0);
              glColor3f(red,green,blue);
                glVertex3f(-size/2 + offsetX,size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,-size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,-size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,size/2 + offsetY,-size/2 + offsetZ);
                // Back face
                glNormal3f(0.0,0.0,-1.0);
              glColor3f(red,green,blue);
                glVertex3f(size/2 + offsetX,size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,-size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(size/2 + offsetX,-size/2 + offsetY,-size/2 + offsetZ);
                // Right face
                glNormal3f(1.0,0.0,0.0);
              glColor3f(red,green,blue);
                glVertex3f(size/2 + offsetX,size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(size/2 + offsetX,-size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(size/2 + offsetX,-size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(size/2 + offsetX,size/2 + offsetY,-size/2 + offsetZ);
                // top face
                glNormal3f(0.0,1.0,0.0);
              glColor3f(red,green,blue);
                glVertex3f(size/2 + offsetX,size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(size/2 + offsetX,size/2 + offsetY,-size/2 + offsetZ);
                // bottom face
                glNormal3f(0.0,-1.0,0.0);
              glColor3f(red,green,blue);
                glVertex3f(size/2 + offsetX,-size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,-size/2 + offsetY,size/2 + offsetZ);
                glVertex3f(-size/2 + offsetX,-size/2 + offsetY,-size/2 + offsetZ);
                glVertex3f(size/2 + offsetX,-size/2 + offsetY,-size/2 + offsetZ);
        glEnd();
}
