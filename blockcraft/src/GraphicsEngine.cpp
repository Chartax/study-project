// Include precompiled headers.
#include <stdafx.h>

// Constructor for graphics engine.
GraphicsEngine::GraphicsEngine(char* title, int x, int y, int w, int h, Uint32 flags)
{
    // Set the various properties from arguments passed to the constructor.
    this->SetTitle(title);
    this->SetPosX(x);
    this->SetPosY(y);
    this->SetWidth(w);
    this->SetHeight(h);
    this->SetFlags(flags);
}

// Start the graphics engine.
void GraphicsEngine::Start()
{
    // Create a window and OpenGL context.
    this->SetWindow(SDL_CreateWindow(this->GetTitle(), this->GetPosX(), this->GetPosY(), this->GetWidth(), this->GetHeight(), this->GetFlags()));
    this->SetContext(SDL_GL_CreateContext(this->GetWindow()));
}

// Flip the engine buffers
void GraphicsEngine::SwapBuffers()
{
    // Swap the GL window.
    SDL_GL_SwapWindow(this->GetWindow());
}

// Limit the game FPS to stop it being faster/slower on faster/slower computers.
// Probably, this should be separated so that it isn't based on FPS and rather tick rate
// on logic exclusively and unrelated to actual graphical performance.
void GraphicsEngine::LimitFPS(int FPS, Uint32 start)
{
    // Get the difference in the number of ticks between the start and now
    Uint32 delta = SDL_GetTicks() - start;
    // Divide 1000 by the number of FPS
    float msPerFrame = 1000 / FPS;

    // If delta is less than the number of frames
    if(delta < msPerFrame)
    {
        // Delay by how many milliseconds delta was greater.
        SDL_Delay(msPerFrame - delta);
    }
}

GraphicsEngine::~GraphicsEngine()
{
    //dtor
}
